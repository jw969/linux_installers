cd installers
work_dir=`pwd`
. ./install_zsh.sh
cd ${work_dir}
. ./install_term-tools.sh
cd ${work_dir}
. ./install_cuda10_on_ubuntu1804.sh
cd ${work_dir}
. ./install_docker_on_ubuntu.sh
cd ${work_dir}
. ./install_nvidia-docker_on_ubuntu.sh
echo "NOTE: Please reboot now for the permission change for docker"
read
cd ..
